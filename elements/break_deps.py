#!/usr/bin/python3
import os
import yaml
import sys

# traverse root directory, and list directories as dirs and files as files
bsts = []
for root, dirs, files in os.walk("."):
    for f in files:
        if f.endswith('.bst'):
            bst = os.path.join(root, f)
            bsts.append(bst)

if './python-restless/python-restless-doc.bst' not in bsts:
    print('WHAIHFIBJHBF')
    
print('Reading .bst files...')
all_elements = set()
depended_on = set()
big_dict = {}
for bst in bsts:
    all_elements.add(bst[2:])
    with open(bst, 'r') as stream:
        data = yaml.load(stream, Loader=yaml.CLoader)
    big_dict[bst[2:]] = data
    for dep in data.get('runtime-depends', []):
        depended_on.add(dep)
leaves = all_elements - depended_on

# NOW BREAK STUFF
print('Loaded {} elements'.format(len(all_elements)))

modified_elements = set()
fixed_elements = set()
def break_loops(seen, pkg):
    if pkg in fixed_elements:
        return
    seen.add(pkg)
    pkgdeps = big_dict[pkg].get('runtime-depends', [])
    #print('Considering Package {} with deps {} for seen of {}'.format(pkg, pkgdeps, seen))
    for dep in [dep for dep in pkgdeps]:
        if dep in seen:
            print('Removed {} from {}'.format(dep, pkg))
            pkgdeps.remove(dep)
            modified_elements.add(pkg)
        else:
            break_loops(seen, dep)
    seen.remove(pkg)
    fixed_elements.add(pkg)

for leaf in leaves:
    break_loops(set(), leaf)
#break_loops(set(),'debian-stack.bst')

print('Writing out {} elements'.format(len(modified_elements)))
for bst in modified_elements:
    with open(bst, 'w') as stream:
        yaml.dump(big_dict[bst], stream, default_flow_style=False)

    
